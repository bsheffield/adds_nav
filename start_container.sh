#!/bin/bash

#starts Jackal container w/ NVIDIA support for graphics display for simulation.
# http://wiki.ros.org/docker/Tutorials/Hardware%20Acceleration
# http://wiki.ros.org/docker/Tutorials/GUI

NAME=jackal:melodic

xhost +local:root

nvidia-docker run -it \
    --privileged \
    --env="DISPLAY" \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    $NAME

export containerId=$(docker ps -l -q)
echo "Container ID:" $containerId

xhost -local:root


