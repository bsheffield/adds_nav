# Autonomous Dry Dock Survey(ADDS) Technical Guide

Technical notes pertaining to the configuration of the ADDS robot.

## Git repositories

Several Git repositories can be found hosted on NAVSEA Fusion SPORK/Gitlab under the 'adds' group located at:  https://spork.navsea.navy.mil/adds/

https://spork.navsea.navy.mil/adds/arduino

https://spork.navsea.navy.mil/adds/ADDS_Nav

https://spork.navsea.navy.mil/adds/waypoint-pusher

https://spork.navsea.navy.mil/adds/adds-safety

TODO topside

TODO obstacle manager

## Network Notes

### Jackal

**Jackal IP:**  192.168.1.100

Username:  nvidia
Password:  drydock

### Microhard Radios

*Note:  The two sets of microhard radios are set exactly the same.  Therefore there will be a network conflict if both sets are powered on.*

**Master:**  192.168.1.1 - Located on Robot

**Slave:**  192.168.1.2 - Used to access Robot remotely

Username:  admin
Password:  drydock

**Velodyne VLP-16 IP Address:**  192.168.1.201

### Add routes

An example that adds a ethernet connected sensor to the route table when a wireless network exists:

```bash
sudo route del -net 192.168.1.0 gw 0.0.0.0 netmask 255.255.255.0 dev eth0
sudo route add -host 192.168.1.7 dev eth0
```

## Commonly Changed Parameters

### ROS Service

The ROS nodes that get started at boot are located in the following file:  **/etc/ros/.../base.launch**

### Velodyne

TODO laserscan, range, min and max height

### Jackal URDF

**jackal.urdf.xacro**

```
  <bridge_plate mount="front" height="0.02" />

  <VLP-16 parent="front_mount" name="velodyne" topic="/velodyne_points" hz="10" samples="440" gpu="false"> 
    <origin xyz="0 0 0.02" rpy="0 0 0"/>
  </VLP-16>
```

### Local and Global Cost Map

**costmap_common_params.yaml**

```footprint: [[0.12, 0.14], [0.12, -0.14], [-0.12, -0.14], [-0.12, 0.14]]```

### DWA Planner

TODO

max_vel
max_rot
yaw_goal_tolerance
latch_xy_goal_tolerance
obstacle infolation
hector max range


## Configuring Services Automatically

* To modify existing services that are started at boot, navigate to the ```/etc/ros``` directory and navigate into it's subfolders to find the launch files.

* To install a service that will automatically run ROS packages, device drivers, and other essentials at boot:

```bash
rosrun robot_upstart install jackal_base/launch/base.launch --job jackal_base
```

## Udev rules

Useful for talking to a low-level device such as an Arduino.  The file can be placed under ```/etc/udev/rules.d/``` folder and the following commands can be used to complete the Udev configuration:

```bash
sudo service udev stop
sudo service udev start
sudo udevadm trigger
```

## Backup/Restore Jetson TX2 image

Place TX2 into recovery mode and plug into the micro-usb port from an external device such as a laptop.  The following commands can be run from an external device terminal to start the backup and restore process.

### Backup

```bash
sudo ./flash.sh -r -k APP -G backup.img jetson-tx2 mmcblk0p1
```

### Restore

```bash
sudo cp backup.img.raw bootloader/system.img
sudo ./flash.sh -r -k APP jetson-tx2 mmcblk0p1
```

