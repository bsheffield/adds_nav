#!/bin/bash


#docker push --insecure-registry spork.navsea.navy.mil spork.navsea.navy.mil/adds/adds_nav

docker build -t spork.navsea.navy.mil/adds/adds_nav --build-arg http_proxy=http://130.109.2.10:8080 --build-arg https_proxy=http://130.109.2.10:8080 -f ../Dockerfile.Jackal-Melodic.Spork ..
