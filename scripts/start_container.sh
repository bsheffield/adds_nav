#!/bin/bash

#starts Jackal container w/ NVIDIA support for graphics display for simulation.
# http://wiki.ros.org/docker/Tutorials/Hardware%20Acceleration
# http://wiki.ros.org/docker/Tutorials/GUI

NAME=jackal:melodic

xhost +local:root

nvidia-docker run -it \
    --env="DISPLAY" \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    --volume="/home/bsheffy/git_repos/work/adds_repos/adds_nav:/catkin_ws/src/adds_nav:rw" \
    $NAME

export containerId=$(docker ps -l -q)
echo "Container ID:" $containerId

xhost -local:root


